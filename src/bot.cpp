#include "bot.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <iomanip>

#include "logger.h"
#include "utils.h"

#include <nlohmann/json.hpp>

#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "httplib/httplib.h"


namespace tg_bot_n {

  void tg_bot_t::init() {
    config.init();
    logger_t::init(config.log, config.level);
    LOGGER(info, "config.level: {}", config.level);
    LOGGER(info, "config.log:   {}", config.log);
    LOGGER(info, "config.token: {}", config.token);
    LOGGER(info, "config.sleep: {}", config.sleep);
  }

  void tg_bot_t::run() {
    TRACER;

    while (true) {
      try {
        step();
      } catch (const std::exception& e) {
        LOGGER(err, "exception: {}", e.what());
        std::cerr << "exception: " << e.what() << std::endl;
      } catch (...) {
        LOGGER(err, "exception: {}", "unknown");
        std::cerr << "exception: " << "unknown" << std::endl;
      }
      std::this_thread::sleep_for(std::chrono::seconds(config.sleep));
    }
  }

  std::string tg_bot_t::request(const std::string& host, const std::string& target) const {
    TRACER;
    LOGGER(debug, "host: {}", host);
    LOGGER(debug, "target: {}", target);

    httplib::SSLClient cli(host.c_str());

    auto res = cli.Get(target.c_str());
    if (!res) {
      LOGGER(err, "error: {}", (int) res.error());
      return {};
    }

    LOGGER(debug, "code: {}", res->status);
    LOGGER(debug, "body: {}\n{}", res->body.size(), res->body);

    return res->body;
  }

  void tg_bot_t::step() {
    static const std::string host = "api.telegram.org";
    std::string target = "/bot" + config.token + "/getUpdates";
    target += "?limit=1";
    target += "&offset=" + std::to_string(config.update_id + 1);
    
    auto body = request(host, target);
    LOGGER(debug, "body: \n{}", body);
    if (body.empty()) {
      return;
    }

    auto json = nlohmann::json::parse(body);
    LOGGER(debug, "json: \n{}", json.dump(2));

    if (!json.contains("result"))
      return;

    for (const auto& result : json.at("result")) {
      LOGGER(info, "json: \n{}", result.dump(2));
      config.update_id    = result.at("update_id");

      if (!result.contains("message"))
        break;

      const auto& message = result.at("message");
      std::string text    = message.at("text");
      size_t message_id   = message.at("message_id");
      size_t chat_id      = message.at("chat").at("id");

      LOGGER(debug, "text: {}", text);

      std::reverse(text.begin(), text.end());

      std::string target = "/bot" + config.token + "/sendMessage";
      target += "?chat_id=" + std::to_string(chat_id);
      target += "&text=" + text;
      target += "&reply_to_message_id=" + std::to_string(message_id);
      auto body = request(host, target);
      auto json = nlohmann::json::parse(body);
      LOGGER(info, "json: \n{}", json.dump(2));
    }
  }
}
