#pragma once

#include <string>

#include "config.h"



namespace tg_bot_n {

  struct tg_bot_t {
    config_t config;

    void init();
    void run();
    void step();

    std::string request(const std::string& host, const std::string& target) const;
  };
}

