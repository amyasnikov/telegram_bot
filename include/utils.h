#pragma once

#include <fstream>
#include <sstream>
#include <stdexcept>
#include <regex>



namespace tg_bot_n {
  template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
  template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;



  static inline std::string str_from_file(const std::string& path) {
    std::ifstream file(path);
    std::string str((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    return str;
  }

  static inline void str_to_file(const std::string& str, const std::string& path) {
    std::ofstream file(path);
    file << str;
  }



  static inline std::string hex(const std::string& str) {
    static const char* digits = "0123456789ABCDEF";
    std::ostringstream ss;
    for (size_t i{}; i < str.size(); ++i) {
      uint8_t c = str[i];
      ss << digits[c >> 4] << digits[c & 0x0F];
      ss << (i % 16 == 15 ? "\n" : " ");
    }
    return ss.str();
  }
}
