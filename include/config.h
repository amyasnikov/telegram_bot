#pragma once

#include <string>



namespace tg_bot_n {

  struct config_t {
    std::string level = "info";
    std::string log   = "-";

    std::string token     = "XXXXXXXXX:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    uint64_t    sleep     = 1;
    uint64_t    update_id = 0;

    void init();
  };
}
