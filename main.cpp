#include <iostream>
#include <csignal>

#include "bot.h"



int main(int argc, char* argv[]) {

  auto signal_handler = [](int signal) {
    std::cerr << "signal: " << signal << std::endl;
    std::exit(EXIT_FAILURE);
  };

  std::signal(SIGTERM, signal_handler);
  std::signal(SIGSEGV, signal_handler);
  std::signal(SIGINT,  signal_handler);
  std::signal(SIGILL,  signal_handler);
  std::signal(SIGABRT, signal_handler);
  std::signal(SIGFPE,  signal_handler);

  try {
    tg_bot_n::tg_bot_t bot;
    bot.init();
    bot.run();
  } catch (const std::exception& e) {
    std::cerr << "exception: " << e.what() << std::endl;
    return 1;
  }

  return 0;
}

