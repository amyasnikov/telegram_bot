#!/bin/bash

VERSION_FILE=version

function get_value {
  local key="$1="
  echo `cat $VERSION_FILE | grep "$key" | head -n 1 | sed -r "s#$key(.*)#\1#" || echo "(unknown)"`
}

function version_major {
  echo `get_value "VERSION_MAJOR"`
}

function version_minor {
  echo `get_value "VERSION_MINOR"`
}

function branch {
  echo `get_value "BRANCH"`
}

function version_patch {
  echo `git rev-list --first-parent --count $(branch)..HEAD 2> /dev/null || echo 0`
}

function version {
  echo $(version_major).$(version_minor).$(version_patch)
}

if [ "$1" == "version" ]; then
  version $@
else
  echo "unknown function: $1"
  exit 1
fi
