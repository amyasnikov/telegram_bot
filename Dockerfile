FROM gcc:12.2 AS image_env

ARG JOBS=1

RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y cmake make libssl-dev rpm

RUN git clone --depth 1 --branch 8.1.1 https://github.com/fmtlib/fmt.git \
  && cd fmt \
  && cmake -S. -B./build -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
  && cmake --build ./build -j $JOBS \
  && cmake --build ./build --target install

RUN git clone --depth 1 --branch v1.9.0 https://github.com/gabime/spdlog.git \
  && cd spdlog \
  && cmake -S. -B./build \
  && cmake --build ./build -j $JOBS \
  && cmake --build ./build --target install

RUN git clone --depth 1 https://github.com/nlohmann/json.git \
  && cd json \
  && cmake -S. -B./build \
  && cmake --build ./build -j $JOBS \
  && cmake --build ./build --target install



FROM image_env as image_builder

COPY . /telegram_bot
WORKDIR /telegram_bot

RUN cmake -S. -B./build \
  && cmake --build ./build -j $JOBS \
  && cmake --build ./build -j $JOBS --target package

ENTRYPOINT ["./build/telegram_bot"]



FROM ubuntu:20.04 as image_compact

RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y libssl-dev ca-certificates

COPY --from=image_builder /telegram_bot/build/*.deb /tmp
RUN apt install ./tmp/*.deb

ENTRYPOINT ["telegram_bot"]

