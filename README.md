# TELEGRAM_BOT

* Author: Alexander Myasnikov
* mailto: myasnikov.alexander.s@gmail.com
* git: https://gitlab.com/amyasnikov/telegram_bot



## Description

* telegram bot sample for kubernetes



## Build

```
docker build -t telegram_bot:latest .
```



## Run

```
docker run --name telegram_bot --env TGBOT_TOKEN=<token> telegram_bot:latest
```



